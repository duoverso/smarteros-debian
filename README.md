# SmarterOS - Debian

## Install Debian
Debian can be downloaded from https://www.debian.org/distrib/netinst. Just choose your platform and download .iso image.

**What to do with .iso image?**

- Load it at [VirtualBox](https://www.virtualbox.org/) or your server
- Write it to USB (flash) drive
    - with [dd](https://linux.die.net/man/1/dd): `dd if=/path/to/file.iso of=/dev/sdX bs=2M`
    - with [ddrescue](https://linux.die.net/man/1/ddrescue): `ddrescue -D -f /path/to/file.iso /dev/sdX`
    - with [Rufus](https://rufus.akeo.ie/) under Windows
- Burn it to CD/DVD

… and boot your device from it.

## Clone this repo and run installation
Next update your packages, install `git`, clone this repository and run `bin/install-smarteros`.

```
sudo apt -y update
sudo apt -y install git sudo
git clone git@gitlab.com:duoverso/smarteros-debian.git
cd smarteros-debian/
sudo bin/install-smarteros
```